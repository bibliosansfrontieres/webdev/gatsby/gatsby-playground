import { Link } from "gatsby"
import React from "react"
import { withStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import Button from "@material-ui/core/Button"
import Typography from "@material-ui/core/Typography"
import LocationOnIcon from "@material-ui/icons/LocationOn"
import LocalOfferIcon from "@material-ui/icons/LocalOffer"
import MenuIcon from "@material-ui/icons/Menu";
import Language from "./language"

const styles = {
  root: {
    flexGrow: "1 !important",
  },
  appbar: {
    backgroundColor: "",
  },
  grow: {
    flexGrow: 1,
    color: "white",
    textDecoration: `none`,
  },
  link: {
    color: `white`,
    textDecoration: `none`,
  },
  pageTitle: {
    color: "white",
  },
}

class Header extends React.Component {
  render() {
    const { classes, title, intl } = this.props
    
    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.appbar}>
          <Toolbar>
            <div className={classes.grow}>
              <Link to="/" className={classes.link}>
                <Typography
                  className={classes.pageTitle}
                  variant="title"
                  component="h1"
                >
                  {title}
                </Typography>
                <Typography
                  className={classes.pageTitle}
                  variant="caption"
                >
                  <Language />
                </Typography>
                
              </Link>
            </div>
            <div>
              <Link to="/tags" className={classes.link}>
                <Button color="inherit" className={classes.grow}>
                  <LocalOfferIcon  fontSize="small"/>
                  {intl.formatMessage({ id: "tags" })}
                </Button>
              </Link>
              <Link to="/location" className={classes.link}>
                <Button color="inherit" className={classes.grow}>
                  <LocationOnIcon  fontSize="small"/>
                  {intl.formatMessage({ id: "locations" })}
                </Button>
              </Link>
              <Link to="/cards" className={classes.link}>
                <Button color="inherit" className={classes.grow}>
                  <MenuIcon fontSize="small"/>
                  {intl.formatMessage({ id: "browse" })}
                </Button>
              </Link>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    )
  }
}

export default withStyles(styles)(Header)
