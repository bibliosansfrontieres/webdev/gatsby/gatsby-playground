import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import { Link } from "gatsby";
import { kebabCase } from "lodash";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import CardHeader from "@material-ui/core/CardHeader";
import Badge from "@material-ui/core/Badge";

const styles = theme => ({
  card: {
    width: 300,
    margin: 10,
    [theme.breakpoints.down('sm')]: {
      width: "90vw",
    },
  }
})

function locationCard(props) {
  const { classes, item } = props

  return (
    <Card className={classes.card}>
        <Link 
        style={{ textDecoration: "none" }}
        to={`/location/${kebabCase(item.location)}/`}
        >
            <CardHeader
                avatar={
                    <Badge badgeContent={item.numberOfCards} color="primary">
                        <LocationOnIcon color="secondary"/>
                    </Badge>
                }
                title={item.location}
            />
        </Link>
    </Card>
  )
}

export default withStyles(styles)(locationCard)
