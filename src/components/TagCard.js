import React from "react"
import { withStyles } from "@material-ui/core/styles"
import Card from "@material-ui/core/Card"
import { Link } from "gatsby"
import { kebabCase } from "lodash"
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import CardHeader from "@material-ui/core/CardHeader";
import Badge from "@material-ui/core/Badge";

const styles = theme => ({
  card: {
    width: 200,
    margin: 10,
    [theme.breakpoints.down('sm')]: {
      width: "42vw",
    },
  }
})

function TagCard(props) {
  const { classes, tag } = props
  return (
    <Card className={classes.card}>
    <Link 
      style={{ textDecoration: "none" }}
      to={`/tag/${kebabCase(tag.tagName)}/`}
      >
        <CardHeader
          avatar={
            <Badge badgeContent={tag.numberOfCards} color="primary">
              <LocalOfferIcon fontSize="small" color="secondary"/>
            </Badge>
            }
          title={tag.tagName}
        />
    </Link>
  </Card>
  )
}

export default withStyles(styles)(TagCard)
