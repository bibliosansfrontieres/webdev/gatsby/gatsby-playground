import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { graphql } from "gatsby";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import Badge from "@material-ui/core/Badge";
import Layout from "../components/layout";
import PageCard from "../components/PageCard";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  flexBoxParentDiv: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    flexFlow: "row wrap",
    justifyContent: "space-between",
    "&::after": {
      content: "",
      flex: "auto",
    },
  },
  locationChips: {
    marginLeft: 10,
    background: "none",
    maxWidth: 330,
  },
  rtlCompat: {
    marginRight: "2px",
  },
})

const TagPage = ({ data, classes, pageContext }) => {
  const itemsWithTag = data.allGoogleSpreadsheetExport.edges

  return (
    <Layout>
      <div className={classes.root}>
        <Typography variant="h4" gutterBottom align="center" component="h1">
          <Badge badgeContent={itemsWithTag.length} color="primary">
          <Chip
            icon={
              <LocalOfferIcon className={classes.rtlCompat}/>
            }
            label={pageContext.tag.toUpperCase().split('/')}
            variant="outlined"
            color="secondary"
            className={classes.locationChips}
            key={pageContext.tag.toString()}/>
          </Badge>
        </Typography>
        <div className={classes.flexBoxParentDiv}>
          {itemsWithTag.map(item => (
            <PageCard item={item} />
          ))}
        </div>
      </div>
    </Layout>
  )
}

export default withStyles(styles)(TagPage)

export const tagPageQuery = graphql`
  query TagPage($tag: String) {
    site {
      siteMetadata {
        title
      }
    }
    allGoogleSpreadsheetExport(filter: { tags: { regex: $tag } }) {
      totalCount
      edges {
        node {
          id
          optimized_thumbnail {
            childImageSharp {
              fluid(maxWidth: 400, maxHeight: 250) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          title
          tags
          location
          category
          icon
          servicesProvided
          optimized_location_image {
            childImageSharp {
              fluid(maxHeight: 50) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`
