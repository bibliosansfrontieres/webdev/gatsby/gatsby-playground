import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { graphql,Link } from "gatsby";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import Chip from "@material-ui/core/Chip";
import Avatar from "@material-ui/core/Avatar";
import PhoneIcon from "@material-ui/icons/Phone";
import Icon from "@material-ui/core/Icon";
import Img from "gatsby-image";
import Divider from "@material-ui/core/Divider";
import { kebabCase } from "lodash";
import Layout from "../components/layout";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  card: {
    width: "50vw",
    margin: "0 auto",
    [theme.breakpoints.down('sm')]: {
      width: "95vw",
    },
  },
  avatar: {
    backgroundColor: "#f47725",
  },
  tagArea: {
    marginRight: 10,
  },
  rtlCompat: {
    marginRight: "2px",
  },
  categoryChips: {
    background: "#fafafa",
    borderRadius: 3,
    marginTop: 10,
  },
})

const ItemPage = ({ data, classes }) => {
  const item = data.googleSpreadsheetExport

  return (
    <Layout>
   <Card className={classes.card}>
      <CardActionArea>
      <CardHeader
        avatar={
          <Avatar className={classes.avatar}>
            {item.title.charAt(0).toUpperCase()}
          </Avatar>
        }
        title={item.title.toUpperCase()}
        subheader={item.openingTimes}
      />
      <Typography variant="overline" color="textSecondary" align="center" component="p" >
      {item.conditions} - {item.beneficiaries}
      <Divider />
      <Link
          to={`/category/${kebabCase(item.category)}/`}
          style={{ textDecoration: "none" }}
          >
            <Chip
              icon={
                <Icon className={classes.rtlCompat} >{item.icon}</Icon>
              }
              label={item.category}
              variant="outlined"
              color="secondary"
              size="small"
              clickable
              className={classes.categoryChips}
            />
        </Link>
      </Typography>
      <Img
          alt={item.title}
          fluid={item.optimized_location_image.childImageSharp.fluid}
      />
      </CardActionArea>
      <CardContent>
        <Typography variant="body1" color="textPrimary" component="p" paragraph="true">
          {item.servicesProvided}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p" paragraph="true">
          {item.comments}
        </Typography>
        <Typography variant="body1" color="textPrimary" component="p" paragraph="true">
        <Link
            to={`/location/${kebabCase(item.location)}/`}
            style={{ textDecoration: "none" }}
          >
            <Chip
              icon={<LocationOnIcon className={classes.rtlCompat} />}
              label={item.location}
              variant="outlined"
              color="secondary"
              size="small"
              clickable
              className={classes.tagArea}
            />
          </Link>
          </Typography>
          <Typography variant="body1" color="textPrimary" component="p" paragraph="true">
            {item.tags.split(",").map(tag => (
              <Link
                to={`/tag/${kebabCase(tag)}/`}
                style={{ textDecoration: "none" }}
              >
                <Chip
                  icon={<LocalOfferIcon className={classes.rtlCompat} />}
                  variant="outlined"
                  color="secondary"
                  size="small"
                  clickable
                  label={tag}
                  key={tag.toString()}
                  className={classes.tagArea}
                />
              </Link>
            ))}
          </Typography>
          <Typography variant="body1" color="textPrimary" component="p" paragraph="true">
          <Chip
                  icon={<PhoneIcon className={classes.rtlCompat} />}
                  variant="outlined"
                  color="secondary"
                  size="small"
                  label={item.contactPerson}
                  key={item.contactPerson.toString()}
                  className={classes.tagArea}
                />
          </Typography>
        </CardContent>
    </Card>
    </Layout>
  )
}

export default withStyles(styles)(ItemPage)

export const ItemPageQuery = graphql`
  query ItemDetails($itemId: String!) {
    googleSpreadsheetExport(id: { eq: $itemId }) {
      id
      optimized_thumbnail {
        childImageSharp {
          fluid(maxWidth: 400, maxHeight: 250) {
            ...GatsbyImageSharpFluid
          }
        }
      }
      title
      tags
      servicesProvided
      conditions
      locationImage
      location
      beneficiaries
      category
      icon
      contactPerson
      openingTimes
      comments
      optimized_location_image {
        childImageSharp {
          fluid(maxHeight: 300) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  }
`
